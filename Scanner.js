import mysql from 'mysql';
import { promisify } from 'util';
import { exec } from 'child_process';
import fetch from 'node-fetch';
import { closeConnection, connection, createConnection } from './Database';
import zlib from 'zlib';
import { getLocalInstalledSoftware } from './Software';

// Create a connection to the MySQL database
await createConnection()

// Promisify the exec function from the child_process module
const execPromise = promisify(exec);

// Retrieve a list of installed software on your computer
async function getInstalledSoftware() {
    try {
        const { stdout } = await execPromise('wmic product get Name');
        return stdout.split('\r\r\n').slice(1, -1);
    } catch (error) {
        console.error(`Failed to retrieve list of installed software: ${error}`);
        return [];
    }
}

// Retrieve the NVD JSON feed
async function getNvdData() {
    try {
        const response = await fetch('https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-recent.json.gz');
        const compressedData = await response.arrayBuffer();
        const data = JSON.parse(zlib.gunzipSync(compressedData));
        return data;
    } catch (error) {
        console.error(`Failed to retrieve NVD data: ${error}`);
        return {};
    }
}


// Automatically create the productNameMapping dictionary from the NVD JSON feed
async function createProductNameMapping() {
    const nvdData = await getNvdData();
    const productNameMapping = {};
    Object.keys(nvdData.CVE_Items).forEach(cveId => {
        const cveData = nvdData.CVE_Items[cveId];
        const products = cveData.configurations.nodes.reduce((acc, node) => {
            if (node.cpe_match) {
                const cpeNames = node.cpe_match.map(match => match.cpe23Uri);
                acc = [...acc, ...cpeNames];
            }
            return acc;
        }, []);
        products.forEach(product => {
            const [prefix, product_body] = product.split(':a:');
            const [vendor, productType, productVersion, productUpdate, productEdition, productLanguage] = product_body.split(':');
            const displayName = `${vendor} ${productType}`;
            productNameMapping[displayName] = productNameMapping[displayName] || [];
            if (!productNameMapping[displayName].includes(product)) {
                productNameMapping[displayName].push(product);
            }
        });
    });
    return productNameMapping;
}

async function scanInstalledSoftwareForCVEs() {
    const installedSoftware = await getLocalInstalledSoftware()/* await getInstalledSoftware(); */
    console.log(`Scanning ${installedSoftware.length} installed software for vulnerabilities...`);
    const query = 'SELECT * FROM products';
    // Execute the SQL query to retrieve all Product data from the MySQL database
    connection.query(query, async (error, results, fields) => {
        if (error) {
            console.error(`Failed to retrieve Product data from MySQL database: ${error}`);
            return;
        }
        const productNameMapping = await createProductNameMapping();
        // Check if any affected products are installed on your computer
        results.forEach(product => {
            /* const affectedProducts = cve.product.split(','); */
            console.log(product.product)
            const matchingDisplayNames = Object.keys(productNameMapping).filter(displayName => {
                return productNameMapping[displayName].includes(product.product);
            });
            if (matchingDisplayNames.length > 0) {
                console.log(`Found vulnerability (${product.cve_id}) affecting: ${matchingDisplayNames.join(', ')}`);
            }

            /* affectedProducts.forEach(affectedProduct => {}); */
        });
        closeConnection()
    });
}

async function searchCPE(cpe) {
  try {
    const response = await fetch(`https://services.nvd.nist.gov/rest/json/cpes/1.0?keyword=${cpe}`);
    const data = await response.json();
    return data;
  } catch (error) {
    console.error(`Failed to retrieve CPE data: ${error}`);
    return {};
  }
}

searchCPE('Android Studio')
  .then((data) => {
    console.log(data.result.cpes);
  });

