import fetch from "node-fetch"
import { CWE_ALLOWED_LIST, OPENCVE } from "./env"
import { sleepWithOutput } from "./Utils"

var username = OPENCVE.username
var password = OPENCVE.password

//Function to fetch cve with cwe filter
export const collectCVEs = async (cweDict) => {

    // Get list of CVEs for every CWE
    for (let cwe in cweDict) {
        const index = cwe
        cwe = cweDict[cwe]
        if (!CWE_ALLOWED_LIST.includes(Number(cwe.id))) continue //remove
        let page = 1  // There might be more than one page with CVEs
        let end = false
        let cveArray = []
        console.log("Starting collecting CVEs in CWE-" + cwe.id)

        // Get the CVEs and stop when 404 is found
        while (true) {
            let res = await fetch(`https://www.opencve.io/api/cwe/CWE-${cwe.id}/cve?page=${page++}`, {
                method: 'GET',
                headers: { 'Authorization': 'Basic ' + Buffer.from(username + ":" + password).toString('base64') }
            })
            if (res.status != 200) {
                if (res.status == 429) {
                    console.log("Error 429: Request limit reached!")
                    console.log("The script will pause for 1 hour.")
                    await sleepWithOutput(60 * 60 * 1000);
                } else {
                    end = true
                }

            }
            res = await res.text()
            if (end) {
                cweDict[index] = {
                    ...cweDict[index],
                    total_cve: cveArray.length,
                    CVEs: cveArray
                }
                console.log("CWE-" + cwe.id + " finished with " + cveArray.length + " CVEs found")
                break
            }

            let body = JSON.parse(res)
            if (body.length) {
                cveArray = [...cveArray, ...body]
            }
            process.stdout.write(' Current page: ' + (page - 1) + '\r')
        }
    }

    return cweDict
}

//Get all info on a specific cve from opencve
export const fetchCVE = async id => {
    let error = false
    var res = null
    do {
        res = await fetch(`https://www.opencve.io/api/cve/${id}`, {
            method: 'GET',
            headers: { 'Authorization': 'Basic ' + Buffer.from(username + ":" + password).toString('base64') }
        })
        if (res.status == 429) {
            console.log("Error 429: Request limit reached!")
            console.log("The script will pause for 1 hour.")
            error = true
            await sleepWithOutput(60 * 60 * 1000);
        } else {
            error = false
        }
    } while (error);

    res = await res.text()
    return JSON.parse(res)

}


export const collectCVEsNVD = async (cweIds) => {
    const NVD_API_URL = 'https://services.nvd.nist.gov/rest/json/cves/1.0';
    const PAGE_SIZE = 100;

    const cweDict = {};
    for (const cweId of cweIds) {
        console.log(`Collecting CVEs for CWE-${cweId}...`);

        let cveArray = [];
        let page = 1;
        let totalPages = null;
        do {
            const url = `${NVD_API_URL}?resultsPerPage=${PAGE_SIZE}&startIndex=${(page - 1) * PAGE_SIZE}&cweId=CWE-${cweId}`;
            const response = await fetch(url);
            const data = await response.json();

            if (totalPages === null) {
                totalPages = Math.ceil(data.totalResults / PAGE_SIZE);
                console.log(`Total pages for CWE-${cweId}: ${totalPages}`);
            }

            cveArray = cveArray.concat(data.result.CVE_Items.map(item => {
                return {
                    id: item.cve.CVE_data_meta.ID,
                    summary: item.cve.description.description_data[0].value,
                    cweId: cweId,
                    cvssV2: item.impact?.baseMetricV2?.cvssV2?.baseScore,
                    cvssV3: item.impact?.baseMetricV3?.cvssV3?.baseScore,
                    publishDate: item.publishedDate,
                    vendors: item?.configurations?.nodes?.map(node => {
                        const cpeMatch = node?.cpe_match?.[0]?.cpe23Uri;
                        if (cpeMatch) {
                            const parts = cpeMatch.split(':');
                            return { vendor: parts[3], product: parts[4] };
                        } else {
                            return null;
                        }
                    }).filter(vendor => vendor !== null)
                };
            }));

            console.log(`Collected ${cveArray.length} CVEs for CWE-${cweId}.`);
            if (page < totalPages) {
                process.stdout.write(`Next page...\r`);
            } else {
                console.log(`Done with CWE-${cweId}.`);
            }

            page++;
        } while (page <= totalPages);

        cweDict[cweId] = { id: cweId, total_cve: cveArray.length, CVEs: cveArray };
    }

    return cweDict;
}

// Example usage:
/* const cweIds = [319, 261];
collectCVEsNVD(cweIds).then(cweDict => {
    console.log('CVEs collection complete.');
    console.log(cweDict);
}).catch(error => {
    console.error(error);
}).finally(() => {
    process.exit();
}); */


export const fetchCVE_NVD = async id => {
    let error = false
    var res = null
    do {
        res = await fetch(`https://services.nvd.nist.gov/rest/json/cve/1.0/${id}`, {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        });
        if (res.status == 429) {
            console.log("Error 429: Request limit reached!")
            console.log("The script will pause for 1 hour.")
            error = true
            await sleepWithOutput(60 * 60 * 1000);
        } else {
            error = false
        }
    } while (error);

    const data = await response.json();
    return data;
};


export const extractCVEDataNVD = (cveData) => {
    const cveId = cveData.cve.CVE_data_meta.ID;
    const summary = cveData.cve.description.description_data[0].value;
    const cweId = cveData.cve.problemtype.problemtype_data[0].description[0].value.split('-')[1];
    const cvssV2 = cveData.impact.baseMetricV2?.cvssV2?.baseScore ?? null;
    const cvssV3 = cveData.impact.baseMetricV3?.cvssV3?.baseScore ?? null;
    const datePublished = cveData.publishedDate;

    const vendors = {};
    const nodes = cveData.configurations.nodes;
    if (nodes && nodes.length > 0) {
        nodes.forEach(node => {
            const vendor = node.cpe_match[0].cpe23Uri.split(':')[3];
            const product = node.cpe_match[0].cpe23Uri.split(':')[4];
            if (vendor in vendors) {
                vendors[vendor].push(product);
            } else {
                vendors[vendor] = [product];
            }
        });
    }

    return {
        cveId,
        summary,
        cweId,
        cvssV2,
        cvssV3,
        datePublished,
        vendors,
    };
}