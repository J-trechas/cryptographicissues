import pkg from 'fetch-installed-software';
import { closeConnection, connection, createConnection } from './Database';
const { getAllInstalledSoftware } = pkg;


export const instertSoftwareDB = async (apps) => {
    var list = [];
    var execTime = new Date()
    var displayNames = []
    for (const index in apps) {
        const app = apps[index];
        if (!app.DisplayName || displayNames.indexOf(app.DisplayName) > -1) continue

        displayNames = [...displayNames, app.DisplayName]
        let formattedInstallDate=null
        if (app.InstallDate)
            formattedInstallDate = app.InstallDate.slice(0, 4) + "-" + (Number(app.InstallDate) ? app.InstallDate.slice(4, 6) : app.InstallDate.slice(5, 7)) + "-" + (Number(app.InstallDate) ? app.InstallDate.slice(6) : app.InstallDate.slice(8))
        list = [...list, [app.DisplayName, app.DisplayVersion, app.Publisher, formattedInstallDate, execTime.toISOString(), execTime.toISOString(), 1]]

    }
    //Connect to DB (REMOVE)
    await createConnection()
    //Insert software into DB
    let stmt = `INSERT INTO installedsoftware VALUES ? ON DUPLICATE KEY UPDATE lastModified=VALUES(lastModified),active=VALUES(active);`;
    await connection.query(stmt, [list], (err, results) => {
        if (err) {
            return console.error(err.message);
        }
        console.log(' Rows inserted in installedSoftware:' + results.affectedRows)
    });
    //Update uninstalled sotfware in DB
    stmt = `UPDATE installedsoftware SET active =0 WHERE lastModified < "${execTime.toISOString()}" ;`;
    await connection.query(stmt, (err, results) => {
        if (err) {
            return console.error(err.message);
        }
        console.log(' Rows set as uninstalled from installedSoftware:' + results.affectedRows)
    });

    //Close DB connection  (REMOVE)
    await closeConnection()
}

export const getLocalInstalledSoftware = async () => {
    var apps = await getAllInstalledSoftware()
    //instertSoftwareDB(apps)
    return apps
}
getLocalInstalledSoftware()
