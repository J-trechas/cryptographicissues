import fetch from "node-fetch"

const collectCWEs = async ()=>{

    // Get Page with the OWASP CWEs
    let html = await fetch('https://owasp.org/Top10/A02_2021-Cryptographic_Failures/')
        .then(res => res.text())

    let listEnabled = false
    let count = 0
    let cweDict = []
    html = html.split('\n')

    // Parse the list of mapped CWEs
    html.forEach(line => {
        if (line.match("https://cwe.mitre.org/data/definitions/")) {
            listEnabled = true
        }

        let cwe = line.match(/CWE-([0-9]{3})\s(.+)\<\/a\>/)
        if (cwe && listEnabled) {
            count += 1
            cweDict = [...cweDict, {
                id: cwe[1],
                Summary: cwe[2],
                total_cve: 0,
            }]
        }
    });

    console.log("\nTotal CWEs collected:" + count)

    return cweDict
}

export default collectCWEs;

