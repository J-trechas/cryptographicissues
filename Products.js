import { connection } from "./Database";

export const insertProductsDB = async (vendors, cve_id) => {
    var list = [];
    for (const vendorIndex in vendors) {
        const vendor = vendors[vendorIndex];
        for (const productIndex in vendor) {
            const product = vendor[productIndex];
            if (product)
                list = [...list, [null, product, vendorIndex, cve_id]]
        }
    }
    if (list.length == 0) return
    ////Insert products into DB
    let stmt = `INSERT INTO products VALUES ? ON DUPLICATE KEY UPDATE product=VALUES(product)`;
    await connection.query(stmt, [list], (err, results) => {
        if (err) {
            return console.error(err.message);
        }
        console.log(' Rows inserted in products:' + results.affectedRows)
    });
}