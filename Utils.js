

export const sleep = async (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export const sleepWithOutput = async (ms) => {
    const remainingTime = ms / 1000;
    let hours = Math.floor(remainingTime / 3600);
    let minutes = Math.floor((remainingTime % 3600) / 60);
    let seconds = Math.floor(remainingTime % 60);

    let output = `Waiting for ${hours}h ${minutes}m ${seconds}s`;
    process.stdout.write(output);

    while (ms > 0) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        ms -= 1000;

        hours = Math.floor(ms / 3600000);
        minutes = Math.floor((ms % 3600000) / 60000);
        seconds = Math.floor((ms % 60000) / 1000);

        output = `\rWaiting for ${hours}h ${minutes}m ${seconds}s  `;
        process.stdout.write(output);
    }

    process.stdout.write('\n');
};
