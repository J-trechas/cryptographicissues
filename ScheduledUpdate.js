import fetch from 'node-fetch';
import zlib from 'zlib';
import { closeConnection, connection, createConnection } from './Database';
import { insertProductsDB } from './Products';
import fs from 'fs';

// Set up file path and default value for last check date
const lastCheckPath = './lastCheckDate.txt';
let lastCheckDate = new Date('2022-02-24');

// Check if the last check date file exists, and read its contents if it does
if (fs.existsSync(lastCheckPath)) {
    const lastCheckStr = fs.readFileSync(lastCheckPath, 'utf8');
    lastCheckDate = new Date(lastCheckStr);
}

// Function to update the last check date and write it to the file
const updateLastCheckDate = () => {
    lastCheckDate = new Date();
    fs.writeFileSync(lastCheckPath, lastCheckDate.toISOString());
}

// Function to check for updates on CVEs
async function checkCveUpdates() {

    await createConnection()
    
    const cweIdsToMonitor = [];
    const query = 'SELECT id FROM cwe;';
    connection.query(query, (error, results, fields) => {
        if (error) {
            console.error(`Failed to retrieve list of CWE IDs to monitor: ${error}`);
            return;
        }
        results.forEach((row) => {
            cweIdsToMonitor.push(row.id);
        });
        console.log(`Retrieved ${cweIdsToMonitor.length} CWE IDs to monitor.`);
    });


    try {
        // Get NVD data
        const response = await fetch('https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-recent.json.gz');
        const compressedData = await response.arrayBuffer();
        const data = JSON.parse(zlib.gunzipSync(compressedData));

        // Loop through CVE items
        Object.keys(data.CVE_Items).forEach(cveId => {
            const cveData = data.CVE_Items[cveId];
            //Extract CWE ids
            const cweIds = cveData.cve.problemtype?.problemtype_data?.[0]?.description?.map(desc => parseInt(desc.value.match(/CWE-(\d+)/)?.[1])) ?? [];
            //console.log(cveData)

            if (cweIdsToMonitor.some(cweId => cweIds.includes(cweId))) {

                // Check if the CVE has been modified since the last check
                const modifiedDate = new Date(cveData.lastModifiedDate);

                if (modifiedDate > lastCheckDate) {
                    // Object to store vendors and their products
                    const vendors = {};
                    // Update the CVE in the database
                    const cveId = cveData.cve.CVE_data_meta.ID;
                    const summary = cveData.cve.description.description_data[0].value;
                    const cvssV2 = cveData.impact?.baseMetricV2?.cvssV2?.baseScore ?? null;
                    const cvssV3 = cveData.impact?.baseMetricV3?.cvssV3?.baseScore ?? null;
                    const productsAffected = cveData.configurations?.nodes?.map(node => node.cpe_match?.[0]?.cpe23Uri).filter(uri => uri !== undefined).length;
                    const datePublished = cveData.publishedDate;
                    // Extract vendor and product data
                    const vendorsData = cveData?.configurations?.nodes?.map(node => {
                        return node?.cpe_match?.map(cpe => {
                            const cpeParts = cpe.cpe23Uri.split(':');
                            const vendor = cpeParts[3];
                            const product = cpeParts[4];
                            return { vendor, product };
                        }) ?? [];
                    })?.flat() ?? [];

                    // Add vendor and product data to the vendors object
                    vendorsData.forEach(({ vendor, product }) => {
                        if (vendors[vendor]) {
                            if (!vendors[vendor].includes(product)) {
                                vendors[vendor].push(product);
                            }
                        } else {
                            vendors[vendor] = [product];
                        }
                    });

                    cweIds.forEach(cweId => {
                        if (!cweIdsToMonitor.includes(cweId)) return

                        const insertQuery = `INSERT INTO cve
                         VALUES (${cveId}, ${summary}, ${cweId}, ${productsAffected}, ${cvssV2}, ${cvssV3}, ${datePublished})
                         ON DUPLICATE KEY UPDATE 
                            summary='${summary}', 
                            products_affected='${productsAffected}', 
                            cvss_v2=${cvssV2}, 
                            cvss_v3=${cvssV3}, 
                            date_published='${datePublished}'`;

                        /* connection.query(insertQuery, (error, results, fields) => {
                            if (error) {
                                console.error(`Failed to update CVE ${cveId}: ${error}`);
                                return;
                            }
                            console.log(`CVE ${cveData.cve.CVE_data_meta.ID} has been updated`);
                        }); */

                    })
                    /* insertProductsDB(vendors,cveId) */
                    console.log(`${cveData.cve.CVE_data_meta.ID} has been modified`);
                }
            }
        });

        // Update the last check date to the current time
        //updateLastCheckDate();
    } catch (error) {
        console.error(`Failed to retrieve NVD data: ${error}`);
    }
    await closeConnection()
}

checkCveUpdates()
// Schedule the check to run once a week
//setInterval(checkCveUpdates, 7 * 24 * 60 * 60 * 1000);
