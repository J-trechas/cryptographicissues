import collectCWEs from "./CWE";
import { collectCVEs, fetchCVE } from "./CVE";
import { closeConnection, connection, createConnection } from "./Database";
import { insertProductsDB } from "./Products";
import { CWE_ALLOWED_LIST } from "./env";



(async () => {
    //Connect to DB
    await createConnection()

    // Get the list of mapped CWEs
    var cweDict = await collectCWEs()

    // add list of CVEs for every CWE
    cweDict = await collectCVEs(cweDict)

    //Format list to insert into DB
    let clone = JSON.parse(JSON.stringify(cweDict))
    let cwelist = clone.map(cwe => {
        delete cwe.CVEs
        return Object.values(cwe)
    })

    //Insert CWEs into DB
    let stmt = `INSERT INTO cwe VALUES ? ON DUPLICATE KEY UPDATE id=VALUES(id),summary=VALUES(summary)`;
    connection.query(stmt, [cwelist], (err, results) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Rows inserted in cwe:' + results.affectedRows);
    });
    // Get all info on every cve and insert into DB
    for (let index in cweDict) {
        let cwe = cweDict[index]

        if (!CWE_ALLOWED_LIST.includes(Number(cwe.id))) continue //remove

        let cvelist = []

        for (let index in cwe.CVEs) {
            try {
                let cve = cwe.CVEs[index];
                let body = await fetchCVE(cve.id)

                //Insert all products affected into DB
                insertProductsDB(body.vendors, cve.id)

                let products_affected = 0;
                for (let index in body.vendors) {
                    let vendor = body.vendors[index];
                    products_affected += vendor.length
                }

                cvelist = [...cvelist, [body.id, body.summary, cwe.id, products_affected, body?.cvss?.['v2'] ?? null, body?.cvss?.['v3'] ?? null, body?.raw_nvd_data?.publishedDate ?? null]]
            } catch (error) {
                console.log(error)
            }
        }

        //Insert CVEs into DB
        if (cvelist.length > 0) {
            let stmt = `INSERT INTO cve VALUES ? ON DUPLICATE KEY UPDATE id=VALUES(id),summary=VALUES(summary),cwe_id=VALUES(cwe_id),products_affected=VALUES(products_affected),cvss_v2=VALUES(cvss_v2),cvss_v3=VALUES(cvss_v3),date_published=VALUES(date_published)`;
            connection.query(stmt, [cvelist], (err, results) => {
                if (err) {
                    return console.error(err.message);
                }
                console.log('Rows inserted in cve:' + results.affectedRows);
            });
        }
    }

    //Close DB connection
    await closeConnection()

})()