import { MYSQLENV } from "./env";
import mysql from "mysql"

export var connection;

//Connect to DB
export const createConnection = async () => {
    connection = mysql.createConnection(MYSQLENV);
    connection.connect((err) => {
        if (err) throw err;
        console.log("Connected!");
    });
}

//Close DB connection
export const closeConnection = async () =>{
    connection.end((err) => {
        if (err) {
            return console.log('error:' + err.message);
        }
        console.log('Close the database connection.');
    });
}